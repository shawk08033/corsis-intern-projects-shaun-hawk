<?php
if(!isset($_SESSION['project_name']) && isset($_SESSION['client_name']))
{
	require('Basecamp.class.php');
	$basecamp = new Basecamp('Corsis');
	$basecamp->setOAuthAuthenticationToken('9d26a2e3fb633f4dfb3a64c57da64dbf2242879');
	$basecamp->setAccount('corsis.basecamphq.com');
	$basecamp->setDebug(true);

	session_start();
	$_SESSION['client_name'] = "";
	$_SESSION['project_name'] = "";
	
	$_SESSION['clients'] = array();
	$_SESSION['projects'] = array();
	
	$_SESSION['_projects'] = json_decode($basecamp->getProjects());
	
	foreach($_SESSION['_projects'] as $project)
	{
		array_push($_SESSION['clients'], $project->{'company'}->{'name'});
	}
}
else if(isset($_SESSION['project_name']) && isset($_SESSION['client_name']))
{
	loadProject();
}

function loadProject()
{
	$_SESSION['client_name'] = $_POST['Clients'];
	$_SESSION['project_name'] = $_POST['Projects'];

}
?>
<html>
<head>
<title> Corsis Project Management </title>
<link rel="stylesheet" type="text/css" href="style.css">
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
</head>
<body>
<div id="logo">
<b>Corsis Project Management Tool</b>
</div>
</br>
</br>
</br>
<div id="main">
<center>
Select a <b>Client</b> &nbsp &nbsp &nbsp &nbsp &nbsp Select a <b>Project</b> </br>
<form action="index.php" method="post">
<select name="Clients" id="Select1" size="11" >
<?php
	for($i = 0; $i < count($clients); $i++)
	{
		echo '<option>'. $_SESSION['clients'][i] . '</option>';
	}
?>
</select>
&nbsp &nbsp &nbsp &nbsp &nbsp 
<select name="Projects" id="Select1" size="11" multiple="multiple">
<script type="text/javascript">
$(document).ready(function()
{
$(".Clients").change(function()
{
var id=$(this).val();
var dataString = 'id='+ id;

$.ajax
({
type: "POST",
url: "getProjects.php",
data: dataString,
cache: false,
success: function(html)
{
$(".projects").html(html);
} 
});

});
});
</script>
</select>
</center>
</div>
<div id="next">
<input type="submit"/>
<div>
</form>
</body>
</html> 