<?php
require('HighriseAPI.class.php');

$hr = new HighriseAPI();
$hr->debug = false; 

$hr->setAccount('corsis');
$hr->setToken('49ad09c6ab2e07203aff770ed642a64d');
?>
<html>
<head>
<title>Corsis Pending Deals For Highrise</title>
<link rel="stylesheet" type="text/css" href="style.css" >
</head>
<body vlink="blue">
<font size="6"> <b> Pending Deals </b> </font> 
<font size="2"> -- worth $ <i>
<?php
$pending_deals = simplexml_load_string($hr->getURL("/deals.xml?status=pending"));
$amount = 0;
foreach($pending_deals->deal as $deal)
{
if($deal->{'duration'} > 1)
{
 $amount += ($deal->{'price'} * $deal->{'duration'});
 }
 else
 {
  $amount += $deal->{'price'};
  }
}
echo $amount;
?>
</i></font>
<div align="right"><a href="javascript:window.print()"><img src="printer.gif" width="16" height="16" /> Print</a></div>
<hr color="black" size="2px" />
<div id="main">
</br>
<?php 
$count = 0;
foreach($pending_deals->deal as $deal)
{
if($deal->{'duration'} > 1)
{
 echo "<img src='Deal_Icon.png'/><font color='blue' size='4'> <b> <a href='http://".$hr->account.".highrisehq.com/deals/".$deal->{'id'}."' target='_blank'>".$deal->{'name'}."</b></font></a> with ". simplexml_load_string($hr->getURL("/parties/search.xml?id=" .$deal->{'party-id'}))->{'party'}->{'name'} ."</br> <b>$" . ($deal->{'price'} * $deal->{'duration'}) ."</b> for <b>". $deal->{'duration'} ."</b> ". $deal->{'price-type'}. "s at <b>$". $deal->{'price'} ."</b>/". $deal->{'price-type'}. "</br>". simplexml_load_string($hr->getURL("/users/" .$deal->{'responsible-party-id'} . ".xml"))->{'name'}." is responsible</br>".$deal->{'category'}->{'name'}."</br> <hr color='gray' size='2px' />";
 }
 else
 {
 if($deal->{'price'} == 0)
 {
	echo "<img src='Deal_Icon.png'/><font color='blue' size='4'> <b> <a href='http://".$hr->account.".highrisehq.com/deals/".$deal->{'id'}."' target='_blank'>".$deal->{'name'}."</b></font></a>with ". simplexml_load_string($hr->getURL("/parties/search.xml?id=" .$deal->{'party-id'}))->{'party'}->{'name'} ."</br> <b> No price set </b> </br>". simplexml_load_string($hr->getURL("/users/" .$deal->{'responsible-party-id'} . ".xml"))->{'name'}." is responsible </br>".$deal->{'category'}->{'name'}." <hr color='gray' size='2px' />";
 }
 else
 {
  echo "<img src='Deal_Icon.png'/><font color='blue' size='4'> <b><a href='http://".$hr->account.".highrisehq.com/deals/".$deal->{'id'}."' target='_blank'>".$deal->{'name'}."</b></font></a> with ". simplexml_load_string($hr->getURL("/parties/search.xml?id=" .$deal->{'party-id'}))->{'party'}->{'name'} ."</br> <b>$" .$deal->{'price'} ."</b></br>". simplexml_load_string($hr->getURL("/users/" .$deal->{'responsible-party-id'} . ".xml"))->{'name'}." is responsible</br>".$deal->{'category'}->{'name'}." <hr color='gray' size='2px' />";
 }
 }
 $count++;
}
echo "Total Pending Deals: <u>" . $count . "</u> worth <u>$" . $amount . "</u>";
?>
</div>
</body>
</html>